<?php include '../au.com.gateway.client/GatewayClient.php'; ?>
<?php include '../au.com.gateway.client.config/ClientConfig.php'; ?>
<?php include '../au.com.gateway.client.component/RequestHeader.php'; ?>
<?php include '../au.com.gateway.client.component/CreditCard.php'; ?>
<?php include '../au.com.gateway.client.component/TransactionAmount.php'; ?>
<?php include '../au.com.gateway.client.component/Redirect.php'; ?>
<?php include '../au.com.gateway.client.facade/BaseFacade.php'; ?>
<?php include '../au.com.gateway.client.facade/Payment.php'; ?>
<?php include '../au.com.gateway.client.payment/PaymentInitRequest.php'; ?>
<?php include '../au.com.gateway.client.payment/PaymentInitResponse.php'; ?>
<?php include '../au.com.gateway.client.root/PaycorpRequest.php'; ?>
<?php include '../au.com.gateway.client.utils/IJsonHelper.php'; ?>
<?php include '../au.com.gateway.client.helpers/PaymentInitJsonHelper.php'; ?>
<?php include '../au.com.gateway.client.utils/HmacUtils.php'; ?>
<?php include '../au.com.gateway.client.utils/CommonUtils.php'; ?>
<?php include '../au.com.gateway.client.utils/RestClient.php'; ?>
<?php include '../au.com.gateway.client.enums/TransactionType.php'; ?>
<?php include '../au.com.gateway.client.enums/Version.php'; ?>
<?php include '../au.com.gateway.client.enums/Operation.php'; ?>
<?php include '../au.com.gateway.client.facade/Vault.php'; ?>
<?php include '../au.com.gateway.client.facade/Report.php'; ?>
<?php include '../au.com.gateway.client.facade/AmexWallet.php'; ?>
<?php 
	$amount = $_POST['amount'];
$merchantReferenceNo=$_POST["merchant_reference_no"];   // Added by Amila 
$client_id = $_POST['client_id'];
$curCode = $_POST['currency_code'];

if ($curCode == 840 || $curCode == "USD") {
    $curCode = "USD";
	$client_id = 14000439;
} elseif ($curCode == 978 || $curCode == "EUR") {
    $curCode = "EUR";
	$client_id = 14000440;
} else {
    $curCode = "LKR";
	$client_id = 14000284;
}
date_default_timezone_set('Asia/Colombo');

//error_reporting(E_ALL); commented by Anjana 
//ini_set('display_errors', 1); commented by Anjana

/*------------------------------------------------------------------------------
STEP1: Build ClientConfig object
------------------------------------------------------------------------------*/
$ClientConfig = new ClientConfig();
$ClientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
$ClientConfig->setAuthToken("b8ec1336-4bd8-4f67-913b-795baf428e29");
$ClientConfig->setHmacSecret("dMQMYJEJReSAyAEk");
$ClientConfig->setValidateOnly(FALSE);
/*------------------------------------------------------------------------------
STEP2: Build Client object
------------------------------------------------------------------------------*/
$Client = new GatewayClient($ClientConfig);
/*-----------------------------------------------------------------------------
STEP3: Build PaymentInitRequest object
------------------------------------------------------------------------------*/
$initRequest = new PaymentInitRequest();
$initRequest->setClientId($client_id);
$initRequest->setTransactionType(TransactionType::$PURCHASE);
$initRequest->setClientRef($merchantReferenceNo);
$initRequest->setComment("merchant_additional_data");
//$initRequest->setTokenize(TRUE);
$initRequest->setExtraData(array("ADD-KEY-1" => "ADD-VALUE-1", "ADD-KEY-2" => "ADD-VALUE-2"));
// sets transaction-amounts details (all amounts are in cents)
$transactionAmount = new TransactionAmount();
$transactionAmount->setTotalAmount(0);
$transactionAmount->setServiceFeeAmount(0);
$transactionAmount->setPaymentAmount($amount);
$transactionAmount->setCurrency($curCode);
$initRequest->setTransactionAmount($transactionAmount);
// sets redirect settings
$redirect = new Redirect();
$redirect->setReturnUrl("http://www.napsl.com/paycorp-client/au.com.gateway.IT/pcw_payment-complete_UT.php");
$redirect->setReturnMethod("GET");
$initRequest->setRedirect($redirect);

/*------------------------------------------------------------------------------
STEP4: Process PaymentInitRequest object 
------------------------------------------------------------------------------*/
$initResponse = $Client->payment()->init($initRequest);

//header('Location:'. $initResponse->getPaymentPageUrl());

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<title>National Association of Photographers</title>

<meta name="description" content="">
<meta name="author" content="">
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,700italic' rel='stylesheet' type='text/css'>
<!-- Js -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/flex-slider-min.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- style switcher -->
<script type="text/javascript" src="/switcher/init.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="/css/skeleton.css" type="text/css" />
<link rel="stylesheet" href="/css/style.css" type="text/css" />
<link rel="stylesheet" href="/css/layout.css" type="text/css" />
<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" /><![endif]-->
<!-- switcher -->
<link rel="stylesheet" href="/switcher/style.css">
<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!-- Favicons -->
<link rel="shortcut icon" href="img/favicon.html">
<style>
.rTable {
	font-family: 'Open Sans' , sans-serif;
	border-radius: 5px;
	background-color: #FFFFFF;
	font-size: 11pt;
	color: black;
}
</style>
</head>
<body>
<!-- boxed wrapper -->
<div class="boxed-wrapper container" style="background-color: #444">
  <!-- end top section -->
  <!-- logo nad ads -->
  
  
  <section class="logo main-section">
    <!-- container -->
    <div class="container">
      <div class="sixteen columns">
        <!-- logo wrapper -->
        <div class="logo-wrapper"> <img src="/img/logo.png" alt="Business" />
          <!-- tagline -->
          	&nbsp;</div>
        <!-- end logo wrapper -->
        <!-- advertise -->
        <div class="advertise"> <img src="/img/ad.png" alt="Advertise" /> </div>
        <!-- end advertise -->
      </div>
      <!-- end sixteen -->
    </div>
    <!-- end container -->
  </section>
  <!-- end logo -->
  <!-- navigation -->
  <section class="navigation-section main-section">
    <!-- container -->
    <div class="container">
      <div class="sixteen columns">
        <!-- nav -->
        <nav class="navigation">
          <ul>
            <li class="first"><a href="#">Open Menu</a></li>
            <li><a href="/index.php">Home</a></li>
            <li><a href="/about-us-napsl.php">About us</a></li>
            <li><a href="/news-events.php">News & Events</a></li>
            <li><a href="/membership.php">Membership</a></li>
            <li><a href="/learn-photography.php">Learn Photography</a></li>
            <li><a href="/contact-us.php">Contact us</a></li>
            <li><a href="/find-a-photographer.php">Find a Photographer</a></li>

          </ul>
        </nav>
        <!-- end nav -->
      </div>
    </div>
    <!-- end container -->
  </section>
  <!-- end navigation -->
  <!-- page content -->
  <section class="page-content main-section">
    <!-- container -->
  <div class="container">
    <!-- left side -->
    <div class="left-side eleven columns">
      <!-- single post -->
      <div class="page-single">
        <!-- main image -->
        <!-- end main image -->
        <!-- post meta -->
        <!-- end post meta -->
        <!-- post content -->
        <div class="post-content section">
          <div class="single-widget widget">
            <div class="widget-content">
              <!-- tabs -->
              <div class="tabs">
                <!-- links -->
                <!-- end links -->
                <!-- posts tab -->
                <div class="posts-tab">
                  <!-- popular posts tab -->
                  <div id="popular-posts-tab0" class="post-tab" style="width: 575px; height: 1142px">
                    <!-- single post -->
                    <!-- end single post -->
                    <!-- single post -->
                    <div class="single-post">
					<div class="rTable">
					<br><br>
                      <table border="0" width="512" cellspacing="0" cellpadding="0" id="table32" height="211">
								<tr>
								<td width="400" colspan="3" align="right"><img src="/img/sampath_paycorp.jpg" alt=""></td>
								</tr>
								<tr>
									<td width="400" colspan="3" height="155">
										
										<br><br>
										<iframe height="400px" width="400px" src="<?php echo $initResponse->getPaymentPageUrl(); ?>">
										
									</td>
								</tr>
								<tr>
									<td width="400" colspan="3">&nbsp;</td>	
								</tr>
															
							</table>
						</div>	
					  <br>&nbsp;
					  </div>
                  </div>
                </div>
                <!-- end posts tab -->
              </div>
              <!-- end tabs -->
            </div>
          </div>
        </div>
		<p>
        <br>
        <!-- end social share -->
        <!-- about  author -->
        <!-- end about author -->
        <!-- [145px cols] -->
        </p>
        <div class="box145 style-default section">
          <!-- header -->
            <h3><br>
			&nbsp;</h3>
        </div>
          <!-- end header -->
          <!-- form -->
          <!-- end form -->
      </div>
      <div class="comments-form section">
      
      
      
          <!-- header -->
			<p>&nbsp;</div>
		<ul>
			 <li>
        <div class="comments-form section">
          &nbsp;</div>
      
      		</li>
        </ul>
		<p>
      
      <!-- end single post content -->
    </div>
    <!-- end leftside -->
    <!-- rightside [sidebar] -->
    <div class="right-side five columns">
      <!-- widgets -->
      <div class="widgets">
        <!-- single widget -->
        <div class="widget">
          <div class="header">
            <h3>Advertise Widget</h3>
			</div>
          <!-- end header -->
          <!-- widget content -->
          <div class="widget-content">
            <!-- ads -->
            <div class="ads-widget-content">
              <!-- single ad -->
              <div class="ad"> <a href="#"><img src="/img/ad_125.png" alt=""></a> <a href="#"><img src="/img/ad_125.png" alt=""></a> <a href="#"><img src="/img/ad_125.png" alt=""></a> <a href="#"><img src="/img/ad_125.png" alt=""></a> </div>
              <!-- end single ad -->
            </div>
            <!-- end ads widget content -->
          </div>
          <!-- widget content -->
        </div>
        <!-- end single widget -->
        <!-- single widget -->
        <!-- end single widget -->
        <!-- single widget -->
        <!-- end single widget -->
      </div>
      <!-- end widgets -->
    </div>
    <!-- end right side -->
  </div>
   
  </section>
  
  <section class="page-content main-section">
    <!-- end container -->
  </section>
  <!-- end page content -->
  <!-- footer -->
  <footer class="footer main-section">
    <!-- widgets -->
    <div class="widgets">
      <!-- widget-container -->
      <div class="widgets-container container">
        <!-- subscribe form -->
        <form class="subscribe-form">
          <a style="display: none;" href="#" class="signup">Sign Up Newsletter</a>
          <input  class="subscribe-email" name="subscribe-email" type="email" required="required" placeholder="Enter Email" value="" />
          <input type="submit" class="submit-subscribe" value="Sign Up" />
        </form>
        <!-- end subscribe form -->
        <div class="sixteen columns">
          <!-- single widget -->
          <div class="widget col300">
            <div class="widget-content">
              <div class="social">
                <h4>Connect With Us</h4>
                <!-- content -->
                <div class="content"> <a href="#" class="facebook"></a> <a href="#" class="twitter"></a> <a href="#" class="dribbble"></a> <a href="#" class="forrest"></a> <a href="#" class="lastfm"></a> <a href="#" class="linkedin"></a> </div>
                <!-- end content -->
              </div>
              <!-- end social -->
            </div>
          </div>
          <!-- end single widget -->
          <!-- single widget -->
          <!-- end single widget -->
          <!-- single widget [flickr] -->
          <!-- end single widget -->
        </div>
        <!-- end sixteen -->
      </div>
      <!-- end widgets container -->
    </div>
    <!-- end widgets -->
    <!-- copyrights -->
    <div class="copyrights container">
      <div class="left columns eight" style="width: 246px; height: 22px">
        Copyrights 2013 - NAPSL<p>Web Design by www.ayselweb.com</p>
		<p>&nbsp;&nbsp;&nbsp;&nbsp; </p>
      </div>
      <!-- end left -->
      <!-- right -->
      <div class="right eight columns" style="width: 690px; height: 22px">
        &nbsp;</div>
      <!-- end right -->
    </div>
  </footer>
  
</body>
</html>