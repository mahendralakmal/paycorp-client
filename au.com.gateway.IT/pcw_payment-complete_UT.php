<?php include '../au.com.gateway.client/GatewayClient.php'; ?>
<?php include '../au.com.gateway.client.config/ClientConfig.php'; ?>
<?php include '../au.com.gateway.client.component/RequestHeader.php'; ?>
<?php include '../au.com.gateway.client.component/CreditCard.php'; ?>
<?php include '../au.com.gateway.client.component/TransactionAmount.php'; ?>
<?php include '../au.com.gateway.client.component/Redirect.php'; ?>
<?php include '../au.com.gateway.client.facade/BaseFacade.php'; ?>
<?php include '../au.com.gateway.client.facade/Payment.php'; ?>
<?php include '../au.com.gateway.client.root/PaycorpRequest.php'; ?>
<?php include '../au.com.gateway.client.root/PaycorpResponse.php'; ?>
<?php include '../au.com.gateway.client.payment/PaymentCompleteRequest.php'; ?>
<?php include '../au.com.gateway.client.payment/PaymentCompleteResponse.php'; ?>
<?php include '../au.com.gateway.client.utils/IJsonHelper.php'; ?>
<?php include '../au.com.gateway.client.helpers/PaymentCompleteJsonHelper.php'; ?>
<?php include '../au.com.gateway.client.utils/HmacUtils.php'; ?>
<?php include '../au.com.gateway.client.utils/CommonUtils.php'; ?>
<?php include '../au.com.gateway.client.utils/RestClient.php'; ?>
<?php include '../au.com.gateway.client.enums/TransactionType.php'; ?>
<?php include '../au.com.gateway.client.enums/Version.php'; ?>
<?php include '../au.com.gateway.client.enums/Operation.php'; ?>
<?php include '../au.com.gateway.client.facade/Vault.php'; ?>
<?php include '../au.com.gateway.client.facade/Report.php'; ?>
<?php include '../au.com.gateway.client.facade/AmexWallet.php'; ?>

<?php
date_default_timezone_set('Asia/Colombo');

//error_reporting(E_ALL); Commented by Anjana
ini_set('display_errors', 0); //Change by Anjana from 1 to 0 

/*------------------------------------------------------------------------------
STEP1: Build ClientConfig object
------------------------------------------------------------------------------*/
$ClientConfig = new ClientConfig();
$ClientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
$ClientConfig->setAuthToken("b8ec1336-4bd8-4f67-913b-795baf428e29");
$ClientConfig->setHmacSecret("dMQMYJEJReSAyAEk");
$ClientConfig->setValidateOnly(FALSE);
/*------------------------------------------------------------------------------
STEP2: Build Client object
------------------------------------------------------------------------------*/

$Client = new GatewayClient($ClientConfig);
/*------------------------------------------------------------------------------
STEP3: Build PaymentCompleteRequest object
------------------------------------------------------------------------------*/
$completeRequest = new PaymentCompleteRequest();
$completeRequest->setClientId(14000284);
$completeRequest->setReqid($_GET['reqid']);
/*------------------------------------------------------------------------------
STEP4: Process PaymentCompleteRequest object
------------------------------------------------------------------------------*/
$completeResponse = $Client->payment()->complete($completeRequest);
/*------------------------------------------------------------------------------
STEP5: Process PaymentCompleteResponse object
------------------------------------------------------------------------------*/

echo '<font face="Arial" size="3"><h1> Thank you ! <h1/>';
echo '<br>Txn Reference : ' . $completeResponse->getTxnReference();
echo '<br>Response Code : ' . $completeResponse->getResponseCode();
echo '<br>Response Text : ' . $completeResponse->getResponseText();
echo '<br>Settlement Date : ' . $completeResponse->getSettlementDate();
echo '<br></font>';
/* -------------Added by Anjana on 05-April-2016----------------------------------*/
/*
$Response_Code = $completeResponse->getResponseCode();
// the message
$msg = '<br>Txn Reference : ' . $completeResponse->getTxnReference().'<br>Response Code : ' . $completeResponse->getResponseCode().'<br>Response Text : ' . $completeResponse->getResponseText().'<br>Settlement Date : ' . $completeResponse->getSettlementDate().'<br>Auth Code : ' . $completeResponse->getAuthCode().'<br>Token : ' . $completeResponse->getToken().'<br>Token Response Text: ' . $completeResponse->getTokenResponseText();
//$msg = "First line of text\nSecond line of text";

// use wordwrap() if lines are longer than 70 characters
$msg = wordwrap($msg,70);
// send email
mail("anjanaw@gmail.com","Sampth IPG Response",$msg);

if($Response_Code==00){
	header('Location: http://ikon.lk/thank-you/');
	exit;
}else{
	header('Location: http://ikon.lk/error/');
	exit;
} */
/*---------------------------------------------------------------------------------*/
?>
